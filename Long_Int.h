#pragma once
#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class Multiplication;

class Long_Int{
public:
	bool negative;
	string number;
	Multiplication *multiplication;

	Long_Int();
	Long_Int(string val);
	string get_number();
	int get_len();
	void rm_zeros();

	Long_Int operator=(Long_Int other);
	Long_Int operator+(Long_Int other);
	bool operator<(Long_Int other);
	Long_Int operator-(Long_Int other);
	Long_Int operator*(Long_Int &other);

	//Making a weak division (but simple)
	Long_Int operator/(Long_Int other);
	friend ostream& operator<<(ostream& os, const Long_Int& val);
	
	void print();
};

class Multiplication {
public:
	virtual Long_Int multiply(Long_Int a, Long_Int b) = 0;
};

class Mult_Karatsuba: public Multiplication{
public:
	Long_Int multiply(Long_Int first, Long_Int other);
};

class Naive_mult :public Multiplication{
public:
	Long_Int multiply(Long_Int a, Long_Int b);
};

class Mult_ToomCook :public Multiplication{
public:
	Long_Int multiply(Long_Int a, Long_Int b);
};

bool test_Miller_Rabin(unsigned long long num, int k);
