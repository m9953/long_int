#include "Long_Int.h"

Long_Int::Long_Int() : number("0"), multiplication(new Naive_mult()), negative(0){}
Long_Int::Long_Int(string val) : number(val), multiplication(new Naive_mult()), negative(0){}

string Long_Int::get_number() {
    return number;
}
int Long_Int::get_len() {
    return get_number().length();
}
void Long_Int::rm_zeros() {
    while (this->number[0] == '0') {
        if (this->get_len() == 1) break;
        this->number = this->number.substr(1, this->get_len() - 1);
    }
}
unsigned long long int get_int(string str) {
    unsigned long long int res = 0;
    for (int i = 0; i < str.length(); i++) {
        res += str[i] - '0';
        if (i != str.length() - 1)
            res *= 10;
    }
    return res;
}
int find_next_pot(int num) {
    int counter = 0;
    do
    {
        if (num % 2 != 0)
            num++;

        num /= 2;
        counter++;
    } while (num / 2 != 0);



    return pow(2, counter);
}
int find_next_poth(int num) {
    int counter = 0;
    do
    {
        num /= 3;
        counter++;
    } while (num != 0);

    if (num == pow(3, counter - 1))
        return num;

    return pow(3, counter);
}

Long_Int oposite(Long_Int& num) {
    num.negative = !num.negative;
    return num;
}

Long_Int Long_Int::operator=(Long_Int other) {
    number = other.number;
    negative = other.negative;
    multiplication = other.multiplication;

    return *this;
}
Long_Int Long_Int::operator+(Long_Int other) {
    Long_Int res;
    int temp = 0;
    bool next_order = false;

    if (negative) {
        if (other.negative) {
            Long_Int num1, num2;
            num1 = *this;
            num1.negative = 0;
            num2 = other;
            num2.negative = 0;

            res = num1 + num2;
            res.negative = 1;
            return res;
        }
        else {
            Long_Int num1;
            num1 = *this;
            num1.negative = 0;

            res = other - num1;
            return res;
        }
    }
    else {
        if (other.negative) {
            Long_Int num2;
            num2 = other;
            num2.negative = 0;

            res = *this - num2;
            return res;
        }
    }

    if (number.length() == max(number.length(), other.number.length())) {
        res.number = number;
        other.number.insert(0, (number.length() - other.number.length()), '0');
    }
    else {
        res.number = other.number;
        number.insert(0, (other.number.length() - number.length()), '0');
    }

    for (int i = res.number.length() - 1; i >= 0; i--) {
        temp += (number[i] - '0') + (other.number[i] - '0');

        if (temp >= 10) {
            res.number[i] = temp % 10 + '0';
        }
        else {
            res.number[i] = temp + '0';
            temp = 0;
        }
        if (i == 0 && temp / 10 != 0)
            next_order = true;
        temp /= 10;
    }

    if (next_order)
        res.number.insert(0, 1, '1');

    return res;
}
bool Long_Int::operator<(Long_Int other) {
    if (get_len() > other.get_len()) {
        other.number.insert(0, (get_len() - other.get_len()), '0');
    }
    else {
        number.insert(0, (other.get_len() - get_len()), '0');
    }

    if (negative && !other.negative) {
        return 1;
    }
    else if (!negative && other.negative) {
        return 0;
    }
    else if (negative) {
        Long_Int num1, num2;
        num1 = *this;
        num1.negative = 0;
        num2 = other;
        num2.negative = 0;

        return !(num1 < num2);
    }

    bool res = 0;
    for (int i = 0; i < get_len(); i++) {
        if (number[i] > other.number[i]) {
            break;
        }
        else if (number[i] < other.number[i]) {
            res = 1;
            break;
        }
    }
    return res;
}
Long_Int Long_Int::operator-(Long_Int other) {
    Long_Int res;

    if (negative) {
        if (other.negative) {
            Long_Int num1, num2;
            num1 = *this;
            num1.negative = 0;
            num2 = other;
            num2.negative = 0;

            res = num1 - num2;
            oposite(res);
            return res;
        }
        else {
            Long_Int num1;
            num1 = *this;
            num1.negative = 0;

            res = num1 + other;
            res.negative = 1;
            return res;
        }
    }
    else {
        if (other.negative) {
            Long_Int num2;
            num2 = other;
            num2.negative = 0;

            res = *this + num2;
            return res;
        }
    }

    if (*this < other) {
        res = other - *this;
        oposite(res);
        return res;
    }

    if (number.length() > other.number.length()) {
        other.number.insert(0, (number.length() - other.number.length()), '0');
    }
    else {
        number.insert(0, (other.number.length() - number.length()), '0');
    }

    //To get || value of difference
    //bool temp = 1;
    //for (int i = 0; i < res.get_len(); i++) {
    //    if (number[i] > other.number[i]) {
    //        break;
    //    }
    //    else if (number[i] < other.number[i]) {
    //        temp = 0;
    //        break;
    //    }
    //}

    //if (*this < other)
    //    return other - *this;

    Long_Int temp = *this;
    res.number = number;

    for (int i = res.number.length() - 1; i >= 0; i--) {
        if (temp.number[i] < other.number[i]) {
            res.number[i] = temp.number[i] + 10 - other.number[i] + '0';
            temp.number[i - 1] -= 1;
        }
        else 
            res.number[i] = temp.number[i] - other.number[i] + '0';
    }

    rm_zeros();
    other.rm_zeros();
    res.rm_zeros();

    return res;
}
//Long_Int Long_Int::operator/(Long_Int other) {
//    Long_Int res;
//    bool minus = !(negative == other.negative);
//    res.number.insert(0, get_len(), '0');
//
//    unsigned long long int num1, num2;
//    num1 = get_int(get_number());
//    num2 = get_int(other.get_number());
//    
//    num1 /= num2;
//    
//    for (int i = res.get_len() - 1; i >= 0; i--) {
//    
//        if (num1 >= 10) {
//            res.number[i] = num1 % 10 + '0';
//        }
//        else {
//            res.number[i] = num1 + '0';
//        }
//    
//        num1 /= 10;
//    }
//    
//    res.rm_zeros();
//    res.negative = minus;
//
//    return res;
//}

//division only for 1-sign numbers
Long_Int Long_Int::operator/(Long_Int other) {
    if (other.number == "0")
        throw "Zero division Error";
    
    Long_Int res;
    int num = get_int(other.number);
    bool minus = !(negative == other.negative);

    res.number.insert(0, get_len() - 1, '0');

    int ost = 0;
    for (int i = 0; i < get_len(); i++) {
        res.number[i] = (number[i] - '0' + ost * 10) / num + '0';
        ost = (number[i] - '0' + ost * 10) % num;
    }

    res.rm_zeros();
    res.negative = minus;
    return res;
}
ostream& operator<<(ostream& os, const Long_Int& val){
    os << val.number;
    return os;
}
void Long_Int::print() {
    if (negative) {
        cout << "-";
    }
    cout << number << endl;
}

//Let's use some cheats :3
//Long_Int Long_Int::operator*(Long_Int other) {
//    int num1, num2;
//    //num1 = stoi(this->get_number());
//    //num2 = stoi(other.get_number());
//    num1 = get_int(this->get_number());
//    num2 = get_int(other.get_number());
//
//    num1 *= num2;
//
//    Long_Int res;
//    res.number.insert(0, get_len() + other.get_len() - 1, '0');
//
//    for (int i = res.get_len() - 1; i >= 0; i--) {
//
//        if (num1 >= 10) {
//            res.number[i] = num1 % 10 + '0';
//        }
//        else {
//            res.number[i] = num1 + '0';
//        }
//
//        num1 /= 10;
//    }
//
//    while (res.number[0] == '0') {
//        if (res.get_len() == 1) break;
//        res.number = res.number.substr(1, res.get_len() - 1);
//    }
//    return res;
//}

Long_Int Long_Int::operator*(Long_Int& other) {
    return multiplication->multiply(*this, other);
}

Long_Int Mult_Karatsuba::multiply(Long_Int first, Long_Int other){
    Long_Int res;
    bool minus = !(first.negative == other.negative);
    int len1 = first.get_len(), len2 = other.get_len();

    if (len1 > len2)
        other.number.insert(0, len1 - len2, '0');
    else
        first.number.insert(0, len2 - len1, '0');

    len2 = first.get_len();
    len1 = find_next_pot(len2);

    if (len1 != len2) {
        first.number.insert(0, len1 - len2, '0');
        other.number.insert(0, len1 - len2, '0');
    }
    //cout << first.number << endl << other.number << endl;

    Long_Int a(first.get_number().substr(0, len1 / 2)), b(first.get_number().substr(len1 / 2, len1 - len1 / 2));
    Long_Int c(other.get_number().substr(0, len1 / 2)), d(other.get_number().substr(len1 / 2, len1 - len1 / 2));
    Long_Int temp = (a + b);

    Long_Int val1, val2, val3;
    if (len1 > 3) {
        a.multiplication = new Mult_Karatsuba();
        b.multiplication = new Mult_Karatsuba();
    }
    else {
        a.multiplication = new Naive_mult();
        b.multiplication = new Naive_mult();
    }

    val1 = a * c;
    val2 = temp;
    temp = (c + d);
    temp.multiplication = a.multiplication;
    val2 = temp * val2;
    val3 = b * d;
    
    //cout << "Val 1 =" << val1.number << "\nVal 2 = " << val2.number << "\nVal 3 = " << val3.number << endl;

    res = val1;
    res.number.insert(res.get_len(), len1 / 2, '0');
    res = res + val2;
    res = res - val1 - val3;
    res.number.insert(res.get_len(), len1 / 2, '0');
    res = res + val3;

    res.multiplication = a.multiplication;
    res.negative = minus;

    return res;
}

Long_Int Naive_mult::multiply(Long_Int a, Long_Int b){
    int temp = 0;
    a.rm_zeros();
    b.rm_zeros();

    if (b.get_len() > a.get_len()) {
        string ns = a.number;
        a.number = b.number;
        b.number = ns;
    }

    Long_Int res, tmp;
    bool minus = !(a.negative == b.negative);
    res.number.insert(0, a.get_len() + b.get_len() + 1, '0');
    tmp.number = res.number;
    
    for (int i = 0; i < a.get_len(); i++) {
        for (int j = 0; j < b.get_len(); j++) {
            temp = (a.number[a.get_len() - i - 1] - '0') * (b.number[b.get_len() - j - 1] - '0');
                 
            //cout << temp << endl;
            int c = 1;
            while (temp != 0){
                tmp.number[tmp.get_len() - i - j - c] = temp % 10 + '0';
                c++;
                temp /= 10;
            }

            res = res + tmp;
            //cout << tmp.number << endl;

            for (int k = 0; k < tmp.get_len(); k++){
                tmp.number[k] = '0';
            }

        }
    }

    res.rm_zeros();
    res.negative = minus;

    return res;
}

Long_Int Mult_ToomCook::multiply(Long_Int first, Long_Int other) {
    Long_Int res;
    bool minus = !(first.negative == other.negative);
    int len1 = first.get_len(), len2 = other.get_len();

    if (len1 > len2)
        other.number.insert(0, len1 - len2, '0');
    else
        first.number.insert(0, len2 - len1, '0');

    len2 = first.get_len();
    len1 = find_next_poth(len2);

    if (len1 != len2) {
        first.number.insert(0, len1 - len2, '0');
        other.number.insert(0, len1 - len2, '0');
    }

    Long_Int a1(first.get_number().substr(0, len1 / 3)), a2(first.get_number().substr(len1 / 3, len1 / 3)), a3(first.get_number().substr(2 * len1 / 3, len1 - 2 * len1 / 3));
    Long_Int b1(other.get_number().substr(0, len1 / 3)), b2(other.get_number().substr(len1 / 3, len1 / 3)), b3(other.get_number().substr(2 * len1 / 3, len1 - 2 * len1 / 3));

    Long_Int r0, r1, r2, r3, r4;
    Long_Int x0, x1, x2, x3, x4;

    Long_Int tmp, tmp1, tmp2;

    Long_Int p1, p2, p3, p4, p5, q1, q2, q3, q4, q5;
    tmp.number = "2";
    tmp1.number = "4";

    p1 = a3;
    p2 = a3 + a2 + a1;
    p3 = a3 - a2 + a1;
    p4 = a3 - a2 * tmp + a1 * tmp1;
    p5 = a1;

    q1 = b3;
    q2 = b3 + b2 + b1;
    q3 = b3 + b1 - b2;
    q4 = b3 - b2 * tmp + b1 * tmp1;
    q5 = b1;
    
    x0 = p1 * q1;
    x1 = p2 * q2;
    x2 = p3 * q3;
    x3 = p4 * q4;
    x4 = p5 * q5;

    tmp1.number = "3";

    r0 = x0;
    r4 = x4;
    r3 = x3 - x1;
    r3 = r3 / tmp1;
    r1 = x1 - x2;
    r1 = r1 / tmp;
    r2 = x2 - x0;
    r3 = r2 - r3;
    r3 = r3 / tmp;
    r3 = r3 + x4 + x4;
    r2 = r1 + r2 - r4;
    r1 = r1 - r3;

    //r0 = a3 * b3;
    //r4 = a1 * b1;
    //r3 = a1 * b2 + a2 * b1;
    //r2 = a1 * b3 + a2 * b2 + a3 * b1;
    //r1 = a2 * b3 + a3 * b2;
    //
    //tmp.number = "2";
    //r3 = a3 + a1 - a2 * tmp;
    //tmp = b3 + b1 - b2 * tmp;
    //r3 = r3 * tmp;
    //tmp = a3 + a2;
    //tmp1 = b3 + b2;
    //tmp = tmp * tmp1;
    //r3 = r3 - tmp;
    //tmp1.number = "3";
    //r3 = r3 / tmp1;
    //
    //tmp1 = a3 - a2;
    //tmp2 = b3 - b2;
    //tmp1 = tmp1 * tmp2;
    //r1 = tmp1 - tmp;
    //tmp2 = 
    

    len1 /= 3;
    res = r4;
    res.number.insert(res.get_len(), len1, '0');
    res = res + r3;
    res.number.insert(res.get_len(), len1, '0');
    res = res + r2;
    res.number.insert(res.get_len(), len1, '0');
    res = res + r1;
    res.number.insert(res.get_len(), len1, '0');
    res = res + r0;

    res.rm_zeros();
    res.negative = minus;
    return res;
}


bool test_Miller_Rabin(unsigned long long int num, int k) {
    if (num == 2 || num == 3)
        return true;
    if (num < 2)
        return false;

    unsigned long long int temp = num - 1;
    int s = 0;

    while (temp % 2 == 0) {
        s++;
        temp /= 2;
    }

    for (int i = 0; i < k; i++) {
        
        int random = rand() % 1000;
        unsigned long long int a = (double)random / 999 * (num - 3) + 2;
        cout << a << endl;
        cout << (double)random / 999 << endl;
        unsigned long long int x = (unsigned long long int) pow(a, temp) % num;
        cout << x << endl;

        if (x == 1 || x == num - 1) {
            continue;
        }
        for (int j = 0; j < s - 1; j++) {
            x = (unsigned long long int) pow(x, 2) % num;
            if (x == 1)
                return false;
            if (x == num - 1)
                break;
        }
        if (x != num - 1)
            return false;
    }
    return true;


}
