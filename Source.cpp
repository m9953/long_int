#include <iostream>
#include "Long_Int.h"

using namespace std;

int main() {
	srand(time(NULL));
	Long_Int a("99999123456789009876543219999912345678900987654321"), b("99999123456789009876543219999912345678900987654321");
	Long_Int kara, naive, cook, minus1, minus2;
	
	//Testing Addition
	a = a - b;
	a.print();

	//Testing Karatsuba
	cout << "-------------------------------------------------------------------------\nKaratsuba ans:\n";
	a.number = "99999123456789009876543219999912345678900987654321";
	b.number = "99999123456789009876543219999912345678900987654321";
	a.multiplication = new Mult_Karatsuba();
	kara = a * b;
	kara.print();

	//Comparing with Naive_Mult
	cout << "-------------------------------------------------------------------------\nNaive_Mult ans:\n";
	a.number = "99999123456789009876543219999912345678900987654321";
	b.number = "99999123456789009876543219999912345678900987654321";
	a.multiplication = new Naive_mult();
	naive = a * b;
	naive.print();

	//Testing stupidus_division
	a.number = "999999";
	b.number = "111";
	cout << "-------------------------------------------------------------------------\nRegular division "<< a << "/" << b << " ans:\n";
	a = a / b;
	a.print();

	//Testing Toom-Cook method
	cout << "-------------------------------------------------------------------------\nToom-Cook ans:\n";
	a.number = "99999123456789009876543219999912345678900987654321";
	b.number = "99999123456789009876543219999912345678900987654321";
	a.multiplication = new Mult_ToomCook();
	cook = a * b;
	cook.print();

	minus1 = kara - naive;
	minus2 = cook - naive;
	cout << "-------------------------------------------------------------------------\nComparing Karatsuba and naive mult answers:\n";
	minus1.print();
	cout << "-------------------------------------------------------------------------\nComparing Toom-Cook and naive mult answers:\n";
	minus2.print();

	a.number = "123";
	b.number = "103";
	cout << "-------------------------------------------------------------------------\nChecking operator > for statement "<< b << " < "
		<< a << "\n";
	cout << (bool)(b < a) << endl;

	/*a.number = "1053";
	b.number = "81";
	a.negative = 0;
	b.negative = 0;
	a = a - b;
	a.print();*/

	//test simple division
	a.number = "0123456789";
	b.number = "3";
	a = a / b;
	cout << "-------------------------------------------------------------------------\nChecking simple division func:\n";
	a.print();

	//Testing Miller-Rabin test
	/*cout << "-------------------------------------------------------------------------\n Testing Miller Rabin func:\n";
	if (test_Miller_Rabin(101, 15))
		cout << "True" << endl;
	else
		cout << "False" << endl;*/
}